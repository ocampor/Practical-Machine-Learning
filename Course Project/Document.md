# Curse Project: Practical Marchine Learning
Ricardo Ocampo  
06/16/2015  


```
## Loading required package: lattice
## Loading required package: ggplot2
```


### Summary 

In this project we predict the capacity of a person to perform barbel lifts correctly and incorrectly in 5 different ways. The information is available in http://groupware.les.inf.puc-rio.br/har. A random forest was trained using cross validation with 10 folds. The concept of out of sample error is explained and it is calculated for the current model. The model is tested with 20 different test cases.

# Data base reading and Preprocessing

The first step is to read the training and validating data bases. Previous, we downloaded the databases from https://d396qusza40orc.cloudfront.net/predmachlearn/pml-training.csv and https://d396qusza40orc.cloudfront.net/predmachlearn/pml-testing.csv in that order. 


```r
training <- read.csv( file = file.path( destPath, testFileName )  )
validate <- read.csv( file = file.path( destPath, trainFileName )  )
```

The dimensions of the training sample are:

```r
dim(training)
```

```
## [1] 19622   160
```

and for the validation sample are:

```r
dim(validate)
```

```
## [1]  20 160
```

As we can see is database with a lot of cases and variables. In order to train a classification model, we need to find which of the variables are numeric. For that, we have to transform the original variables into numbers. When categorical variables are transformed into numeric, all the values are coerced to NA's. This is a rapid way to filter the variables that are not useful.


```r
classe <- training[[ ncol(training) ]]
training <- as.data.frame( lapply( X = training, 
                                   FUN = function(x) as.numeric( as.character(x) ) ) )
```

Therefore, after we transformed the variables, we look for the variables that contain less than 90% of missing values and we retain those variables.


```r
nas <- sapply( X = training, 
               FUN = function(x) ( sum( is.na(x)/ length(x) ) ) < 0.9 )
training <- training[ nas ]; training <- training[ 5:ncol(training )]
training$classe <- classe
```

The database is so big to train a classification algorithm with all the cases. Therefore, we need to divide the training database into different chunks. One for training, and the other ones to test our model. For that, the training database will contain 10% of the cases, and the rest will be divided into 20 different databases. Thus we can test the model.


```r
idx <- createDataPartition(y = training$classe, p = 0.1, list = F )
testing <- training[-idx,]
idx_t <- createDataPartition( y = testing$classe, times = 20, list = T )
```


# Training Classification Algorithm and Cross Validation

We selected random forest. Therefore, the libary caret is used to train the algorithm. The dependent variable is the class and there are used all the numeric features to predict it. K-fold cross validation with 10 folds is used to train the algorithm. 


```r
fit <- train( classe ~ . , 
              data = training[ idx ,],
              trControl = trainControl( method = "cv", number = 10),
              method = 'rf' )
```

```
## Loading required package: randomForest
## randomForest 4.6-10
## Type rfNews() to see new features/changes/bug fixes.
```

```r
fit$finalModel
```

```
## 
## Call:
##  randomForest(x = x, y = y, mtry = param$mtry) 
##                Type of random forest: classification
##                      Number of trees: 500
## No. of variables tried at each split: 2
## 
##         OOB estimate of  error rate: 5.91%
## Confusion matrix:
##     A   B   C   D   E class.error
## A 548   3   2   4   1  0.01792115
## B  18 337  23   2   0  0.11315789
## C   0  16 321   6   0  0.06413994
## D   3   0  19 297   3  0.07763975
## E   1   4   6   5 345  0.04432133
```


# Results

The final model is the following:

```r
fit
```

```
## Random Forest 
## 
## 1964 samples
##   52 predictors
##    5 classes: 'A', 'B', 'C', 'D', 'E' 
## 
## No pre-processing
## Resampling: Cross-Validated (10 fold) 
## 
## Summary of sample sizes: 1768, 1768, 1768, 1768, 1767, 1767, ... 
## 
## Resampling results across tuning parameters:
## 
##   mtry  Accuracy   Kappa      Accuracy SD  Kappa SD  
##    2    0.9393865  0.9232613  0.01499617   0.01901408
##   27    0.9393736  0.9232419  0.01967981   0.02494069
##   52    0.9276854  0.9084451  0.01734575   0.02199088
## 
## Accuracy was used to select the optimal model using  the largest value.
## The final value used for the model was mtry = 2.
```

Once the classification algorithm is trained, we test it using 20 different test data bases. For that, we first define the accuracy function that calculates all the correct classifications and divides it by the total of cases. The results are shown following.

```r
accuracy <- function( x, fit, data ){
  sum( predict(fit, data[x,] ) == data[x,]$classe ) / length( data[x,]$classe )
}
acc <- sapply( X = idx_t, FUN = accuracy, fit = fit, data = testing )
acc
```

```
## Resample01 Resample02 Resample03 Resample04 Resample05 Resample06 
##  0.9430351  0.9392978  0.9398641  0.9405436  0.9394111  0.9379388 
## Resample07 Resample08 Resample09 Resample10 Resample11 Resample12 
##  0.9442809  0.9392978  0.9408834  0.9414496  0.9389581  0.9413364 
## Resample13 Resample14 Resample15 Resample16 Resample17 Resample18 
##  0.9407701  0.9412231  0.9432616  0.9397508  0.9385051  0.9381653 
## Resample19 Resample20 
##  0.9423556  0.9391846
```

Also, it is calculated a global accuracy, averaging the results of the 20 test databases.

```r
mean( acc )
```

```
## [1] 0.9404757
```

### Validation Test

Finally, we predict using the validate database.

```r
predict( fit, validate )
```

```
##  [1] B A B A A E D B A A C C B A E E A B B B
## Levels: A B C D E
```

### Out of sample error

When training a classification model, there is a chance that the model can be overfitted. Therefore, the in sample error that we have is . Nevertheless, when we test our model with independent data, we can get worse results. Hence, we tested the model with 20 different independent databases, and the resulting out of sample error is the folloging.


```r
1 - acc
```

```
## Resample01 Resample02 Resample03 Resample04 Resample05 Resample06 
## 0.05696489 0.06070215 0.06013590 0.05945640 0.06058890 0.06206116 
## Resample07 Resample08 Resample09 Resample10 Resample11 Resample12 
## 0.05571914 0.06070215 0.05911665 0.05855040 0.06104190 0.05866365 
## Resample13 Resample14 Resample15 Resample16 Resample17 Resample18 
## 0.05922990 0.05877690 0.05673839 0.06024915 0.06149490 0.06183465 
## Resample19 Resample20 
## 0.05764439 0.06081540
```

