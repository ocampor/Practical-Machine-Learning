###########################################################################
#####                     Classification Trees                        #####
###########################################################################
data(iris)
library(ggplot2)
library(caret)
# Verifies the frequency of each class
table(iris$Species)
# Creates training and test sets
inTrain <- createDataPartition( y = iris$Species,
                                p = 0.7, list = FALSE )
training <- iris[inTrain,]
testing <- iris[-inTrain,]
dim(training); dim(testing)

# Plots the classes and two variables
qplot(Petal.Width, Sepal.Width, colour=Species, data=training)

# Trains a classification tree
modFit <- train(Species ~ ., method = "rpart", data = training)
# See the divisions
print(modFit$finalModel)

# Plots the classification plot
install.packages("rattle")
install.packages("rpart.plot")
library("rattle")
library("rpart.plot")
fancyRpartPlot(modFit$finalModel)

# Predict results
predict(modFit, newdata = testing)

###########################################################################
#####                          Bootstraping                           #####
###########################################################################
install.packages("ElemStatLearn")
library("ElemStatLearn")
data(ozone, package="ElemStatLearn")
ozone <- ozone[ order(ozone$ozone), ]
head(ozone)
## Bagged loess
ll <- matrix( NA, nrow = 10, ncol = 155 )
for( i in 1:10 ){
  ss <- sample( 1:dim(ozone)[1], replace = T )
  ozone0 <- ozone[ss, ]; ozone0 <- ozone0[order( ozone0$ozone), ]
  loess0 <- loess( temperature ~ ozone, data = ozone0, span = 0.2 )
  ll[i,] <- predict( loess0, newdata = data.frame( ozone = 1:155) )
}
## Plot bagged loess
plot( ozone$ozone, ozone$temperature, pch = 19, cex = 0.5 )
for( i in 1:10 ){ lines( 1:155, ll[i,], col = "grey", lwd = 2 )}
# Plot bagged loess curve
lines( 1: 155, apply( ll, 2, mean ), col = "red", lwd = 2)

## Bagging in caret
install.packages( "party")
library("party")
predictors = data.frame( ozone = ozone$ozone )
temperature = ozone$temperature
treebag <- bag( predictors, temperature, B = 10,
                bagControl = bagControl( fit = ctreeBag$fit,
                                         predict = ctreeBag$pred,
                                         aggregate = ctreeBag$aggregate ) )
## Plots results
plot( ozone$ozone, temperature, col = 'lightgrey', pch = 19 )
points( ozone$ozone, predict( treebag$fits[[1]]$fit, predictors ), pch = 19, col = "red" )
points( ozone$ozone, predict( treebag, predictors ), pch = 19, col = "blue" )

###########################################################################
#####                     Random Forests                              #####
###########################################################################
data(iris); library(ggplot2); library(caret)
inTrain <- createDataPartition( y = iris$Species, 
                                p = 0.7, list = F )
training <- iris[ inTrain, ]
testing <- iris[ -inTrain, ]

## Train random forest
modFit <- train( Species ~ ., data = training, method = "rf", prox = T)
modFit
## Gets a single tree
getTree( modFit$finalModel, k = 2)
## Gets class centers
irisP <- classCenter( training[,c(3,4)], training$Species, modFit$finalModel$proximity )
irisP <- as.data.frame(irisP); irisP$Species <- rownames( irisP )
p <- qplot( Petal.Width, Petal.Length, col = Species, data = training )
p + geom_point( aes( x = Petal.Width, y = Petal.Length, col = Species),
                size = 5, shape = 4, data = irisP )
## Predicting new values
pred <- predict( modFit, testing); testing$predRight <- pred == testing$Species
table( pred, testing$Species )
## Plots predictions
qplot( Petal.Width, Petal.Length, colour = predRight, 
       data = testing, main = "newdata Predictions")


###########################################################################
#####                        Boosting                                 #####
###########################################################################
install.packages("ISLR")
library(ISLR); data(Wage); library(ggplot2); library(caret);
Wage <- subset( Wage, select= -c(logwage) )
inTrain <- createDataPartition( y = Wage$wage,
                                p = 0.7, list = F )
training <- Wage[inTrain,]; testing <- Wage[ -inTrain, ]

## Fits the boosting model
modFit <- train( wage ~ ., mothod = "gbm", data= training, verbose = F )
print( modFit )

## Plots the results
qplot( predict( modFit, testing ), wage, data = testing )


###########################################################################
#####                   Model Based Prediction                        #####
###########################################################################
data(iris); library(ggplot2); library(caret)
names(iris)
inTrain <- createDataPartition(y = iris$Species,
                               p = 0.7, list = F )
training <- iris[ inTrain ,]
testing <- iris[ -inTrain, ]
dim(training)
dim(testing)

## Build predictions
modlda = train( Species ~ ., data= training, method = "lda")
modnb = train( Species ~ ., data = training, method = "nb" )
## Prediction
plda = predict( modlda, testing ); pnb = predict( modnb, testing )
table( plda, pnb)

equalPredictions = (plda == pnb)

qplot( Petal.Width, Sepal.Width, colour = equalPredictions, data= testing )